# Tejas Software Projects - Implementing DevOps Proposal

This document provides a solution for implementing DevOps for *all Tejas Developed Software*, including Salesforce Commercecloud (SFCC) clients. We first give an overview of how the software build & release manual process is currently done. The solution is based on using [Open-source GoCD](https://www.gocd.org/why-gocd/) for modeling, defining & executing devops pipelines. GoCD is selected for its unique (in all open-source offerings) feature on [Value Stream Mapping (VSM)](https://www.gocd.org/getting-started/part-3/). One or more *tasks* in GoCD pipeline might invoke *Ansible playbook* and is executed by the Ansible orchestrator (a.k.a Controller) to configure the specific target machine in the Liquid Web infrastructure. Or, a task could invoke just a bash or Powershell script to do the required actions. The Ansible playbook & other script logs are stored as part of the GoCD pipeline run logs. The artifacts produced from the pipeline run could be stored in Azure cloud storage or locally in the GoCD server machine itself (TBD).

While writing this solution architecture, the author has tried out the proposed components in the solution and found out that the basic things are working as proof-of-concept. All of the code tried out including the pipelines & playbooks are available in [this bitbucket repo](https://bitbucket.org/appanp_tejas/tjsprods-devops-demo). There are a few open-issues on the details of GoCD & Ansible which are listed in the end.
## Changes in this Document Compared to v3

Listed below are the major changes compared to [version 3]():

1. Added the "Installation & Setup Instructions - Ansible, GoCD & Windows" section.
1. Added the "GoCD Pipelines - Tejas Products & SFCC" section.
1. Added the "Pipeline Analytics - Devops Metrics" section - which is not complete (TODO).
1. Created separate section for Open Issues & task list section has reference to gdocs excel sheet.
1. Modified the solution overview diagram to add clarity on where each component runs.
1. Other minor edits

## Changes in this Document Compared to v2

List below are the major changes in this proposal document compared to [version 2](https://tejassoftech-my.sharepoint.com/:b:/g/personal/appanp_tejassoftware_com/EVC0CerZhrJLuLU7PQT4vDEBNSoMnRv8bV4y-Aoz-9fRjw):

1. Introduction of GoCD as the CI/CD server & modify the solution architecture to reflect this.
1. Removed "Bitbucket pipelines" section & other references and replaced with GoCD pipeline.
1. Removed "Ansible Tower" section since Ansible Tower is a paid product and it will not be required since there are only a small number of ansible hosts to manage.
1. Added a new section "GoCD Overview" which gives the basics about GoCD.

## Installation & Setup Instructions

The installations & setup can be grouped into the following:

1. Setup of Windows machine(s) which are to be managed using Ansible. This requires WinRM to be enabled.
    * These steps are listed in [this document](./ansible_windows_host_git_repo_access_setup.md)
1. Setup of Linux machine to run the Ansible controller (using Python) and ansible command to run playbooks.
    * Install Python 3.x using linux distribution package manager - rpm or apt.
    * Install ansible using the command: `pip3 install ansible`.
    * Setup the VM network adapter so that it is accessible from Windows host.
    * Setup the shared folder for installing the GoCD agent which becomes accesible from both Windows & Linux.
1. Installation & setup of GoCD server and Agent with Java runtime. The steps for doing this are given below. _GoCD is to be setup in the DEVWEB12 machine in the same Linux VM_ as above.

Installation steps for GoCD server & agent in the instance DEVWEB12-Linux are:

1. Install the proper version of Java (v15.x) from [Oracle Java downloads page](https://www.oracle.com/java/technologies/downloads/)
1. Download & install the GoCD server from [this link](https://docs.gocd.org/current/installation/install/server/linux.html)
1. Set the Go agent configuration in the file:  
   `$GO_INSTALL_HOME/wrapper-config/wrapper-properties.conf`
    1. Set the `wrapper.java.command=$INSTALL_ROOT/jdk-15.0.2/bin/java`
    1. Set the server URL: `wrapper.app.parameter.101=http://<go-server-hostname>:8153/go`
        * Set `<go-server-hostname>` to`localhost` if they are in the same machine.
1. Install the PostgreSQL (above v9.6) using the rpm package & yum package installer.
    * Create database for GoCD with access to user roles & credentials.
1. Setup GoCD server to use installed database with required credentials (DB connection string).
1. Setup at least one GoCD agent since Ansible playbook can be invoked only from Linux.
    * Follow the instructions in [this page](https://docs.gocd.org/current/installation/install/agent/linux.html) to set up the agent & service in DEVWEB12-Linux instance.
    * Make sure that the newly installed agent is register with server (from GoCD agents web page).

## Current Build, Test & Deployment Processes

The projects are categorised into Products, SFCC Projects & Others. Integration & UAT Testing is mostly done manually in most of the projects, some projects might have unit-test suites.

### For Tejas Products

The current deployment (or publish) process follows a checklist based manual process as documented in [SCM_Publish_Checklist_2021Sep20.xlsx](https://tejassoftech-my.sharepoint.com/:x:/g/personal/ramantr_tejassoftware_com/EdR3VAjQCOpMrmQH33XMHUIBRyYsVAe641OhK2X1XPNw6Q). This manual process is carried out by a team of people and it takes quite some time. There are around 20 steps excluding the patch update (#8), databases backup (#9), application backups (#10) and post deployment verification (#23). Time-consuming operations such as database backups should be avoided during production release deployment if they are not required, to avoid long down-time of systems.

Release team is in the process of writing powershell/shell scripts for some of these steps. Most of these steps can be automated using *already existing Ansible roles, modules & collections* (available in Ansible galaxy), which are used in playbooks. This will avoid re-inventing the wheel and spending additional time. Some of the custom steps such as enabling/disabling SFCC jobs, etc. will require writing custom Ansible modules or roles

### For SFCC Projects

Currently uses Bitbucket pipelines which are triggered for check-ins. Deployment is done using custom pipelines which creates the zipped code version files with cartridges and transfers it to the Staging instance in SFCC cloud.

### For Other Projects

Not sure how this is done for the projects which *do not* fall into the above two categories, so they are *not covered in this proposal*.

## Solution Overview

The diagram below shows the proposed devops solution. Since Tejas Products currently *do not run on .NET Core*, even to run the unit test suite requires building the applications in a development or staging machine (in Liquid Web) which has the proper versions of platform dependencies. Once the Tejas products are migrated to run on .Net Core, we should be able to build & run the unit-tests using Bitbucket pipelines itself. Features or systems which can be used in future are shown with a *star in brackets* (\*).
*Staging environment* is *not currently used* and might be required (advised to have) if we need to reproduce production bugs, since development should be having the environment required for next release. Hence you might not be able to use development environment.

![Solution Overview Diagram](./imgs/tejas_prods_devops_oview.svg)

In the sub-sections below, we elaborate on the specific components listed below which are part of the solution.

1. LiquidWeb IT Infrastructure: Windows host machine which runs applications, database servers, etc. including the network.
1. Ansible Orchestrator Engine & Playbooks.
1. Bitbucket Git Repositories, specifically the *IaC Repo* which has the Ansible related code, devops pipelines (in GoCD YAML form) & other configuration files.
1. GoCD Agents & Pipelines and Other deployment features.
1. GoCD Web UI for user/group adminstration, pipeline run status & VSM analytics.
1. Azure Cloud as artifact repo & documentation server (TBD).

### IT Infrastructure maintained by LiquidWeb

All of the Tejas products run on Windows Server currently and they will be the resources or targets to be controlled by Ansible. To communicate to a Windows host and use Windows modules, the Windows host must meet these requirements:

1. Ansible can generally manage Windows versions under current and extended support from Microsoft.
    * Ansible can manage desktop OSs including Windows 7, 8.1, and 10.
    * Ansible can manage server OSs including Windows Server 2008, 2008 R2, 2012, 2012 R2, 2016, and 2019.
1. Ansible requires PowerShell 3.0 or newer.
1. At least .NET 4.0 to be installed on the Windows host.
1. A WinRM listener should be created and activated. 

Please refer to the [Setup Ansible Windows Host & Bitbucket Repo Access](https://bitbucket.org/appanp_tejas/tjsprods-devops-demo/src/master/docs/ansible_windows_host_git_repo_access_setup.md) document for the setup steps to be followed in Windows & Bitbucket Web UI. 

Check out the official documentation of [setting up a Windows Host](https://docs.ansible.com/ansible/latest/user_guide/windows_setup.html) if any of the above requirements are not satisfied.

Liquidweb also has a API called [Storm API](https://cart.liquidweb.com/storm/api/docs/bleed) which can be used for changing the VM for existing client or for creating new VMs for a new tenant. It looks like there are no Ansible roles or modules specifically for LiquidWeb configuration. There is also a [LiquidWeb CLI](https://github.com/liquidweb/liquidweb-cli/releases) written in Go, not sure if this a wrapper for Storm API, but has commands to configure Liquidweb assets.

### Ansible Orchestrator Engine, Playbooks & Vault

Since the build & deployment scripts have to run on the IT infrastructure, we need a way to execute scripts remotely. We also need something to keep track of the configuration of the systems and their state. These needs are fulfilled by [Ansible Orchestration Engine](https://www.ansible.com/overview/how-ansible-works) and the configurations can be defined in Ansible playbooks. Refer to the [Ansible Overview](#ansible-overview) section which goes into some detail on how to define playbooks. Please refer to more detailed official documentation which are linked in that section.

Ansible also provides a mechanism to encrypt secrets and keep it under version control called [Vault](https://docs.ansible.com/ansible/2.8/user_guide/vault.html). Then these secret fields can be referenced from the playbook. Note that the encrypted secrets file content also has a file-level password and this can be configured as encrypted environment variable in GoCD pipelines. This approach avoids storing multiple secrets each as one single environment variable. Check out [this detailed step-by-step hands-on article](https://www.digitalocean.com/community/tutorials/how-to-use-vault-to-protect-sensitive-ansible-data-on-ubuntu-16-04) on how to use the `ansible-vault` command and how to reference them in config files as "vault variables".

### Bitbucket Git Repositories

All the applications code is version-controlled in git repos of Bitbucket, which could also contain the unit-test suite. There might be separate repositories for:

* Integration testing code & configuration files which are required for testing only.
* API testing code & config files required for this tests.
* End-to-end UI testing code & config files.

Apart for the application related repos, we also need to have a separate repo for infrastructure configuration which has to follow the structure expected by Ansible. This repo is referred to as *IaC Repo* in the figure. Currently, the code repos manitained for SFCC clients have their pipeline definition files, instead of in a separate repo and this can be followed while using GoCD pipelines as well.

### GoCD Agents & Pipelines

GoCD is a CI/CD server written in Java and has most of the features to model & implement [DevOps pipelines](http://martinfowler.com/bliki/DeploymentPipeline.html) with [manual approval gates](https://www.gocd.org/2017/05/23/control-deployments-manual-approvals/). It has two components: [GoCD Server](https://docs.gocd.org/current/installation/installing_go_server.html) which delegates pipelines to the [GoCD agents](https://docs.gocd.org/current/configuration/managing_a_build_cloud.html) and also provides the [web interface](http://localhost:8153/go/pipelines) for defining new pipelines & administering the GoCD server.

As shown in the solution architecture diagram, we  have planned to run *GoCD server in DEVWEB12 machine* in a separate Linux Virtual Machine (VM). Linux is used since Andible controller requires linux, the components which run on Linux VM are shown with  <span style="background:#ff99cc;color:#000000">this color fill</span>. The GoCD agents can be run on Linux or Windows since it is also written in Java, but we need the pipelines which use ansible to run on only "linux agents" (agents running on linux) and this can done using *resources which are just plain text tags*. GoCD [matches the resources required with the resources provided by agents](https://docs.gocd.org/current/configuration/managing_a_build_cloud.html#matching-jobs-to-agents) so that a *pipeline is scheduled to only run on agents which have at least the resources required for that job*.

Please refer to [GoCD Overview section](#gocd-overview) in this document for more details on GoCD basics. If you need more information, please refer to [their product documentation](https://docs.gocd.org/current/) or the [community discussion forum](https://groups.google.com/g/go-cd).

### GoCD Web UI and VSM Analytics

GoCD server implements a Web UI which can be used for viewing all the defined pipelines & their status in a dashboard, creating pipelines, listing the [materials (which are nothing but pipeline triggers)](https://docs.gocd.org/current/introduction/concepts_in_go.html#materials) defined and administering the GoCD instance in terms of [users, roles and their permissions](https://docs.gocd.org/current/configuration/managing_users.html). GoCD supports a [plugin architecture with extension points](https://docs.gocd.org/current/extension_points/plugin_user_guide.html) for extending the functionality and there are [OAuth authorisation plugins](https://www.gocd.org/plugins/#authorization) which *does not currently support Bitbucket for user authentication* (See issue #3).

The [VSM Analytics Plugin](https://github.com/gocd/gocd-analytics-plugin#readme) provides pipeline analytics to find out [which steps in the devops process are creating bottlenecks](https://www.ibm.com/garage/method/practices/discover/practice_value_stream_mapping/) in execution. These analytics data can also be used to track [four key metrics](https://cloud.google.com/blog/products/devops-sre/using-the-four-keys-to-measure-your-devops-performance) as defined by the [DORA Research Program](https://www.devops-research.com/) and these are listed below & covered in these blog posts: [part-1](https://www.gocd.org/2018/10/30/measure-continuous-delivery-process/), [part-2](https://www.gocd.org/2018/11/30/deployment-frequency/) and [part-3](https://www.gocd.org/2019/01/14/cd-metrics-deployment-lead-time.html):

1. Lead time: cumulative lapsed time it took from start to deployment. This is less than *Cycle time* which also includes the implementation time for analysing, planning & developing the feature.
1. Deployment frequency: the number of times deployment happened, after the feature is ready for integration & UAT testing.
1. Mean Time To Restore (MTTR): this is the mean time to restore services in production.
1. Change fail percentage: percentage of failed deployments.

More detailed description of these metrics are there in [this blog post](https://www.gocd.org/2018/01/31/continuous-delivery-metrics/). The last two metrics are *stability metrics* and CD pipeline data alone doesn't provide enough information to determine what a deployment failure with real user impact is. Stability metrics only make sense if they include data about real incidents that degrade service for the users, which might be tracked in production incident bug tracking tool.

### Azure Cloud as Artifacts Repository & Doc Server

Use of an *Artifact repository* is advised for *consistent distribution of versioned packages or libraries consistent* across your development team members. Nuget streams, npm/maven repository are all examples of artifact repositories which are public. For example, if a developer has to reproduce a production bug in his/her machine, she needs to know which versions of the packages has to be used & install those specific versions with the specific version of the application.

Azure cloud, specifically storage is used for the following needs:

1. *Nuget static feeds of artifacts*: If the developers need to refer to common libraries from a solution, we can do that using [Sleet](https://github.com/emgarten/Sleet) after uploading the artifacts to Azure storage.
    * Steps to be done to set up Azure feed is available [here](https://github.com/emgarten/Sleet/blob/main/doc/feed-type-azure.md)
    * The commands `sleet push` and `sleet download` for pushing artifacts and downloading them respectively.
1. *Static Web Assets*: Not sure how you publish & cache the Javascript & HTML assets today, but after build & test, these can be uploaded to Azure storage and exposed as public web folder.
1. *API Documentation*: If you need to automatically generate REST API documentation from API specs file (e.g., Swagger, OpenAPI), one can write a separate pipeline with the commands to build the docs and upload it to the Azure storage or a Bitbucket/Github repo (as website) so that it can be accessed from the web.
1. *Reports generated by automated Test Runs*: These can also be pushed to blob storage & served as web pages.

## GoCD Pipelines - Tejas Products & SFCC Projects

We need to create the GoCD pipelines to trigger the build & deploy process whenever there are commits to the git repos maintained in Bitbucket. The _Deployment pipelines_ only run when there is a release label attached to the commit, specific branch named `release/*` or version file is updated or it is manually triggered. The _Build & Unit Test Pipelines_ run for every commit to check if the code version committed is not breaking the build & the developer(s) are informed as soon the build is broken.

### Tejas Products

All of the Tejas products using .NET, we need to build & test the committed code in a server (DEVWEB12-Win) which has the .NET environment available. This is done in a pipeline which has the stages listed below:

1. Compile & Build stage: either using MSBuild tool or dotnet command line tool.
1. Unit test stage: using one of the .Net unit test framework.
1. Report publish stage: unit test & coverage reports sre published.

For each of the repo (TOMS, MyPO and WMS), there will be a _GoCD material_ which triggers the pipeline. If the commands to be executed for each of the stages above are the same, we could have a single pipeline which has all the three or more git commit trigger as materials. Refer to [this pipeline](https://bitbucket.org/appanp_tejas/tjsprods-devops-demo/src/master/gocd/pipelines/dotnetcore-bld.gocd.yaml) for an example.

The release deployment pipeline will have all the same stages as in build & test pipeline above but with the following additions or changes:

1. (Optional) End-to-end automation testing stage.
1. Manual stage or gate for approving the manual test results.
1. Manual stage for deploying the build to production instance.
    * This could have some tasks which are automated by invoking ansible playbook.
### SFCC Projects

For SFCC projects, we need the nodejs environment with possibly other tools or packages. The stages are more or less the same except that there might be bash shell scripts specific to each SFCC repo. Deployment of cartridges (as a zip file) is just copying to the remote webdav directory in target instance (which could be one of Development, Staging or an integration sandbox) and making the code version active. 

Release deployment to production requires setting up a code & data replication job and triggering them. This can be a manual stage in the pipeline.

## Pipeline Analytics - Devops Metrics

We have listed the four main metrics to be tracked for improving the process. These can be tracked in the analytics dashboard once we have pipelines run-time data collected. We can also see if the pipeline run historical data in Bitbucket can be import into the GoCD analytics database tables.

<h2><a name="ansible-overview">Ansible Overview</a></h2>

<img align="right" alt="Ansible Architecture" src="./imgs/ansible_arch.png" width="400" height="330">

Ansible is an [Infrastructure as Code (IaC)](https://www.redhat.com/en/topics/automation/what-is-infrastructure-as-code-iac) tool for managing the IT infrastructure. Unlike the other IaC tools (Chef and Puppet), Ansible uses an agent-less approach and is easier to learn. Even though this is not a complete tutorial on Ansible, we introduce the conecpts used and other references to learn from, in the sub-sections below. You can start with this article on [Ansible: Quick-start guide, Review & Notes from LinuxAcademy](https://medium.com/@rahimc/ansible-quick-start-guide-review-notes-from-linuxacademy-7de3cee40c7a) and the associated [Ansible Quick-start guide in LucidChart format](https://lucid.app/lucidchart/0c4c899b-39e1-430e-8186-c8b8812b6888/view?page=0_0#).

As in the diagram above, Ansible uses a *controller machine* (which has to be linux) for managing the other Windows, Linux or public cloud instances, which are called *hosts*. The Ansible orchestrator (invoked using `ansible` or `ansible-playbook`) converts the playbook into Python script, transfers (using `scp`) to the remote machines & executes it there.

<br clear="right"/>

### Playbooks

[Playbooks](https://docs.ansible.com/ansible/latest/user_guide/playbooks_intro.html) are the main entry point for Ansible and they consist of one or more *Plays* in ordered list. Each play consists of multiple *Tasks* which are executed in sequence. Each play executes part of the overall goal of the playbook, running one or more tasks. Each task calls an *Ansible module*.

[YAML](https://docs.ansible.com/ansible/latest/reference_appendices/YAMLSyntax.html) based [DSL](https://docs.ansible.com/ansible/2.3/playbooks_directives.html) is used for writing a playbook. The example below shows a simple playbook to execute a powershell script in `dev1-uswest` host. For some examples, please refer to [Ansible examples repo](https://github.com/ansible/ansible-examples).

```
- name: Run powershell script
  hosts: dev_webserver 
  gather_facts: false
  tasks:
    - name: Run powershell script
      script: files/helloworld.ps1
```

A playbook is executed by invoking the `ansible-playbook` command with comma separated hostnames or [inventory file](https://docs.ansible.com/ansible/latest/user_guide/intro_inventory.html) which are the target of the configuration. This DigitalOcean tutorial on [How to Write Ansible Playbooks](https://www.digitalocean.com/community/tutorial_series/how-to-write-ansible-playbooks) has 11 sections and is a good starting point for writing playbooks.

### Facts, Templates & Variables

Ansible can retrieve or discover certain variables containing information about your remote systems or about Ansible itself. [Variables](https://docs.ansible.com/ansible/latest/user_guide/playbooks_vars_facts.html) related to remote systems are called *facts*. With facts, you can use the behavior or state of one system as configuration on other systems. For example, you can use the IP address of one system as a configuration value on another system. Variables related to Ansible are called magic variables.

Templates are typically used to set up configuration files, allowing for the use of variables and other features intended to make these files more versatile and reusable. Ansible uses the [Jinja2](https://pypi.org/project/Jinja2/) templating engine which allows expressions in the playbook for string manipulation, list filtering, etc. For a complete list of expression possible, refer to [this Templating Page](https://docs.ansible.com/ansible/latest/user_guide/playbooks_templating.html).

Ansible uses [variables](https://docs.ansible.com/ansible/latest/user_guide/playbooks_variables.html) to manage differences between systems. With Ansible, you can execute tasks and playbooks on multiple different systems with a single command. To represent the variations among those different systems, you can create variables with standard YAML syntax, including lists and dictionaries. You can define these variables in your playbooks, in your inventory, in re-usable files or roles, or at the command line. You can also create variables during a playbook run by registering the return value or values of a task as a new variable.

### Playbook Modules

[Modules](https://docs.ansible.com/ansible/latest/user_guide/modules_intro.html) - also referred to as *task plugins* or *library plugins* - are discrete units of code that can be used from the command line or in a playbook task. Ansible executes each module, usually on the remote managed node, and collects return values. In Ansible 2.10 and later, most modules are hosted in collections.

One of the ways to invoke a module is to directly use it in command-line as `ansible webservers -m service -a "name=httpd state=started"`. These are called *adhoc commands*. A better way to invoke the same `service` module is through a task step in a playbook, like the following:

```yaml
- name: restart webserver
  service:
    name: httpd
    state: restarted
```

[This all modules page](https://docs.ansible.com/ansible/2.8/modules/list_of_all_modules.html) has an index of all available modules. For more on modules, please view the [Packt video on Ansible playbook modules](https://www.youtube.com/watch?v=avwoj01RseA).

### Roles & Ansible Galaxy

[Roles](https://docs.ansible.com/ansible/latest/user_guide/playbooks_reuse_roles.html) are a way to promote reusability by packaging data file, templates, tasks & others. One or more roles can be just referrenced from a playbook, using the `roles` keyword. Roles let you automatically load related vars, files, tasks, handlers, and other Ansible artifacts based on a *known file structure*. After you group your content in roles, you can easily reuse them and share them with other users.

[Ansible Galaxy](https://docs.ansible.com/ansible/latest/galaxy/user_guide.html) refers to the [Galaxy web site](https://galaxy.ansible.com) a free site for finding, downloading, and sharing community developed roles.

Some more references on Ansible roles:

1. [Ansible Roles, Medium article](https://vibhanshusharma13.medium.com/ansible-roles-8fbb0fafb49)
1. [Introduction to Ansible Roles lesson](https://www.udemy.com/course/ansible-essentials-simplicity-in-automation/learn/lecture/6125476#overview) in the free Udemy course from Redhat.


<h2><a name="gocd-overview">GoCD Overview</a></h2>

<img align="right" alt="GoCD Architecture" src="./imgs/gocd_pipeline_as_code.svg" width="400" height="330">

GoCD CI/CD server has better features (compared to lightweight Bitbucket pipelines or Github actions) for defining deployment pipelines. Go-CD server instance schedules pipelines on multiple agents which can run on different machines. One can also [run multiple agents](https://github.com/gocd/gocd/blob/master/installers/go-agent/release/README-running-multiple-agents.md) on the same machine and it requires separate installation folders. An agent can have [resources](https://docs.gocd.org/current/introduction/concepts_in_go.html#resources) specified if you want to make certain pipelines run on specific agents. An agent has one or more [environments](https://docs.gocd.org/current/introduction/concepts_in_go.html#environment) specified so that the associated pipelines use that environments for deployment. A pipeline can be associated with *only one* environment. [Environment Variables](https://docs.gocd.org/current/introduction/concepts_in_go.html#environment_variables)can be defined at multiple levels: Within environments, pipelines, stages and jobs and are made available when the tasks in the job runs. The artifacts produced by the jobs are stored in an *artifacts folder* configured in the GoCD XML configuration file available at: `$GOCD_INSTALL_DIR/config/cruise-config.xml`. Since the artifact sizes can be big, it is advised to store them in a separate disk partition with the GoCD database and log files.

GoCD also has extension points and [plugin API](https://plugin-api.gocd.org/current/) defined which are used to build custom plugins. There is a [directory of currently available plugins](https://www.gocd.org/plugins/) which are of a few different types: Analytics, Artifact, Authorization, Configuration, Elastic Agents, Notification, Package, SCM, Secrets and Tasks. Some plugins come bundled with the GoCD server, other plugins can be installed by copying the plugin JAR file into `$GOCD_INSTALL_DIR/plugins/external/` folder.

<img align="right" alt="Pipeline Stage Structure" src="./imgs/stage_with_jobs_tasks.png" width="300" height="330">

The figure to the right shows a stage of a pipeline with the output artifacts. Some of the pipeline related [concepts](https://docs.gocd.org/current/introduction/concepts_in_go.html) used in GoCD are listed below:

1. *[Pipeline](https://docs.gocd.org/current/introduction/concepts_in_go.html#pipeline)*: refers to a devops pipeline, e.g., build and unit-test of code. One can define a pipeline in a YAML or JSON file and store it in a git repo and these definitions will be automatically imported if they are version controlled in a [configuration repo](https://docs.gocd.org/current/advanced_usage/config_repo.html).
1. *[Stage](https://docs.gocd.org/current/introduction/concepts_in_go.html#stage)*: first-level of pipeline definition, e.g., build and unit-test pipeline can be defined to have two stages: build and unit-test stages. The stages in a pipeline are executed in sequence. A stage can be set to manual by setting the truning off the `Trigger on completion of previous stage` setting.
1. *[Job](https://docs.gocd.org/current/introduction/concepts_in_go.html#job)*: A job is part of a stage and it *runs independently of other jobs defined in the stage*. e.,g., build stage can have separate jobs to build the web app code and another to build front-end code. Job timeout can be specified in minutes at a global level and also specifically for each job. In the job settings, the artifacts produced by the tasks can be copied to a specific location in artifacts folder of server.
1. *[Task](https://docs.gocd.org/current/introduction/concepts_in_go.html#task)*: A task is part of a job and it is usually a single command. There can be a *sequence of tasks in a job*. If a task in a job fails, then the job is considered failed, and unless specified otherwise, the rest of the tasks in the job will not be run. Recommendation: Keep your task simple to just one command. If a task does too many things and if it fails, one will not know where it failed (in the dashboard) and will have to dig into the task run log to find out the exact failing step. There are a few *built-in tasks* such as Ant, Nant, etc. and [new custom tasks](https://docs.gocd.org/current/extension_points/task_extension.html) can be defined in *task plug-ins*.
1. *[Material](https://docs.gocd.org/current/introduction/concepts_in_go.html#materials)*: A material is a trigger for a pipeline and it can be either a timer, code repo change or a stage in another pipeline. Multiple materials are allowed in which case any of them will trigger the pipeline.
1. *[Fan-in and Fan-out](https://docs.gocd.org/current/introduction/concepts_in_go.html#fan_in_out)*: *Fan-in* refers to a pipeline material depending on multiple upstream pipelines. Dependent pipeline will be triggered only when all upstream stages are completed. *Fan-out* refers to a single pipeline stage trigerring multiple downstream pipelines.

## Next Steps as Project Tasks

For the current list of tasks with their status, please refer to [this Excel file in Google Docs](https://docs.google.com/spreadsheets/d/1AGWG7NWyyDtkodvpAOLZAL5Iifzc7ieS4Gp7u4euapk/edit?usp=sharing). Please refer to Version-01 in version history of the document.

## Open Issues

The following are the open-issues which we could face while doing the above tasks:

1. For security reasons, we cannot support ssh. Instead WinRM will be used and WinRM traffic source can be restricted to nodes within Liquidweb infrastructure.  
   ~~Check if ssh is allowed to Liquidweb instances? Not sure why there is a specific `lw ssh` command in the Liquidweb CLI. Need to check if direct ssh is allowed~~.
1. Since bitbucket pipelines are not used for deployment, this is irrelevant.  
    ~~Check if the bitbucket provided deployment rollback works & is sufficient for us.~~
1. Check how to implement OAuth plugin for Bitbucket for user authorisation.
    * [Bitbucket OAuth2 Implementation Page](https://developer.atlassian.com/cloud/bitbucket/oauth-2/)
    * [GoCD plugin for integrating Crowd - git repo](https://github.com/mgambini/crowd-integration-plugin)
1. How to integrate Redmine release version state change with GoCD pipeline trigger?
    * A few project teams use Redmine roadmap version feature to plan releases.
    * A release in Redmine can be in one of *3 states: Open, locked or closed*.
1. Manual Testing as a Gate: Currently the testing team does manual testing before release deployment & attaches the document in Redmine ticket.
    * If it is available in its own git repo will it be possible to trigger the release deployment pipeline when the test results document is available in repo & also the manual gate is approved?
## Minutes from Chakra's Meeting on 09/22

1. SSIS and SSRS Packages: what are they? Where it is version-controlled? etc.
1. Need to get the Redmine page Chakra has created infra/wiki - DONE
1. Deployment diagram - Dev machine (App/DB server), etc.
1. There are a few jobs which need to stopped & started as pre & post conditions of deployment.
1. What is SCM management tool?
     * Pulling the code from bitbucket will work in Jenkins

## Other References

1. [Actionable continuous delivery metrics](https://www.gocd.org/assets/images/campaign/ebook-cd-analytics/ebook-cd-analytics.pdf), GoCD Team, 2018
1. Documentation for [ansible.windows Collection in Ansible Gallery](https://docs.ansible.com/ansible/latest/collections/ansible/windows/index.html#plugins-in-ansible-windows)
1. Documentation for [community.windows Collection in Ansible Gallery](https://docs.ansible.com/ansible/latest/collections/community/windows/index.html#plugins-in-community-windows)
1. [How to provision an Azure SQL Database using Ansible](https://www.sqlshack.com/how-to-provision-azure-sql-database-using-ansible/)
1. [How do I get started with an Ansible playbook?](https://searchitoperations.techtarget.com/answer/How-do-I-get-started-with-an-Ansible-playbook)
1. [How to Manage Multistage Environments with Ansible](https://www.digitalocean.com/community/tutorials/how-to-manage-multistage-environments-with-ansible), Dec 2016.
1. [Configuration Management 101: Writing Ansible Playbooks](https://www.digitalocean.com/community/tutorials/configuration-management-101-writing-ansible-playbooks), May 2019.
1. [How To Set Up Ansible Inventories](https://www.digitalocean.com/community/tutorials/how-to-set-up-ansible-inventories)
1. [Execute .exe on Windows with Ansible](https://stackoverflow.com/questions/27862267/execute-exe-on-windows-with-ansible/41750827)
1. [Hosting your own Nuget feeds](https://docs.microsoft.com/en-us/nuget/hosting-packages/overview), Microsoft documentation.
1. [How to host a NuGet v3 feed on Azure storage](https://emgarten.com/posts/how-to-host-a-nuget-v3-feed-on-azure-storage) - introduces [open-source Sleet](https://github.com/emgarten/Sleet)
1. [Continuous Delivery: GoCD Vs. Spinnaker](https://www.gocd.org/2017/07/10/gocd-vs-spinnaker/), July 2017.
1. [GoCD integration with LambdaTest](https://www.lambdatest.com/support/docs/gocd-integration-with-lambdatest/)