# Setup Ansible Windows Host & Bitbucket Repo Access

This document details steps to setup the Ansible windows host machine and also Bitbucket for git repo access from Ansbile playbook. *Bitbucket pipeline is assumed to be the CI/CD server*, but this could be your own self-hosted GoCD server in which case certain steps will differ slightly. These changes in the installation steps are also *not indicated* since they just depend on the operating system of the host - windows or linux.

**Open Issues:** 

1. ~~Note that the setup of the ssh keys and the ssh command arguments depends on the approach to be taken for ssh access. There are few approaches as discussed in [this blog post](https://leftasexercise.com/2019/12/23/using-ansible-with-a-jump-host/) for connecting to ssh server behind firewall.~~
1. WinRM requires opening up the HTTPS port 5986 on the windows host machines, we need to check if this can done?
## Setting up Windows Host for Ansible

The following steps are to prepare the windows host for working with Ansible controller using WinRM connection. These commands have to be executed in Powershell *as a user belonging to Admin group*:

1. Make sure you have Powershell version 3.0 at least.
1. Download the powershell script for setting up WinRM including powershell remoting, from [this Ansible link](https://github.com/ansible/ansible/blob/devel/examples/scripts/ConfigureRemotingForAnsible.ps1).
1. Execute the downloaded script in Powershell:  
```
Set-ExecutionPolicy Unrestricted
& "~\Downloads\ConfigureRemotingForAnsible.ps1"
Set-ExecutionPolicy Restricted
```
4. This step might be already done in the script above, so removing it.  
   ~~Enable & configure WinRM using the following commands (taken from [this ansible doc page](https://docs.ansible.com/ansible/latest/user_guide/windows_setup.html#winrm-setup))~~:
```
[Net.ServicePointManager]::SecurityProtocol = [Net.SecurityProtocolType]::Tls12
$url = "https://raw.githubusercontent.com/ansible/ansible/devel/examples/scripts/ConfigureRemotingForAnsible.ps1"
$file = "$env:temp\ConfigureRemotingForAnsible.ps1"

(New-Object -TypeName System.Net.WebClient).DownloadFile($url, $file)

powershell.exe -ExecutionPolicy ByPass -File $file
```

5. Removing this step since this might be required only for ssh access.  
   ~~Set Powershell as the default login shell:~~
```
Write-Host 'Set the default shell to PowerShell'
Set-ItemProperty `
    -Path 'HKLM:\SOFTWARE\OpenSSH' `
    -Name DefaultShell `
    -Value "C:\Windows\System32\WindowsPowerShell\v1.0\powershell.exe"
```

6. ~~In inventory file, also add `ansible_shell_type='powershell'`.~~
1. Removing this step since not using ssh.  
   ~~Make sure that the ssh daemon (`sshd`) is running as a service and enabled.~~
```
GetService "sshd"
Start-Service "sshd"
```
4. Install the WinRM python module for using `winrm connection`, using command:  
   `pip install "pywinrm>=0.2.2"`
1. Test the setup from Ansible controller using the command below to ping the windows host:  
   `ansible -vvv win -i win_hosts -m win_ping`

The sample hosts inventory file `win_hosts`, used in the command above, is as below:
```
[win]
192.168.0.102

[win:vars]
ansible_user=IEUser
ansible_password=xxxxxxxxxx
ansible_connection=winrm
ansible_winrm_server_cert_validation=ignore

```
## Setting up ssh Access from Windows Host Machine to Bitbucket

This is required for cloning the git repo in bitbucket *without password using ssh*. This is documented in detail for different operating systems in [this bitbucket page](https://support.atlassian.com/bitbucket-cloud/docs/set-up-an-ssh-key/). The steps are also given below:

Note that these steps are taken from [this Microsoft page on OpenSSH key management](https://docs.microsoft.com/en-us/windows-server/administration/openssh/openssh_keymanagement)

1. Generate ssh key pair using: `ssh-keygen -t ed25519`. Passphrase can be left empty.
1. By default ssh-agent is disable, enable it using PS command: `Get-Service ssh-agent | Set-Service -StartupType Manual`
1. Start the ssh-agent service using command: `Start-Service ssh-agent`.
1. Add the ssh private key (*without the .pub extension*) using the command: `ssh-add ~\.ssh\id_ed25519`.  
   File name `id_ed25519` could be different if you had given a different (meaningful) name in Step.1 above.
1. Addition of ssh public key has to be done in Bitbucket UI path: `Profile (Bottom-left icon) -> Personal Settings -> SSH Keys -> Add Key (button)`:
      * Add the contents of the public key (generated in Step.1 above), default name: `id_ed25519.pub` into the text box & save.
1. Test if the ssh connectivity works using: `ssh -T -i <private-key-file> <bitbucket-user-id>@bitbcuket.org`.

## ~~Setting up ssh Access from Bitbucket to Windows Host~~

The steps are the same as in the section above except the ssh key-pair has to be generated in Bitbucket & added to the repo from which bitbucket pipeline is invoked.

1. This has to be in Bitbucket UI: Go to the SSH Keys section in repository page:  
    `Repo home page -> Repository Settings -> SSH Keys (under PIPELINES) -> Generate Keys -> Copy Public Key`
1. Copy the public key generated in Step.1 above & deploy it in the windows host as explained in `Deploying the public key -> Admin user` section of [this help page](https://docs.microsoft.com/en-us/windows-server/administration/openssh/openssh_keymanagement).
1. In the same page in Step.1, after giving the hostname, get the host's fingerprint.
1. Create a sample playbook which does the equivalent of `git --version` command.
     * This has to be invoked from a custom pipeline in the repo & check if it works.

