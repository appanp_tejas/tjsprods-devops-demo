[![MIT license](http://img.shields.io/badge/license-MIT-brightgreen.svg)](http://opensource.org/licenses/MIT)

This repo has code from [this AspNetCoreSpa github repo](https://github.com/asadsahi/AspNetCoreSpa) and has .Net Core based API, PWA front-end implemented in Angular and test suite for unit, API & UI Testing.
This main reason that this repo exists is to **demo the DevOps solution proposed in [this documet](docs/tejas_current_tobe.md)**.

This application code requires a few dependencies. For the specific technologies used, see the list below:

- [ASP.NET Core 5.0](http://www.dot.net/)
- [Entity Framework Core 5.0](https://docs.efproject.net/en/latest/)
  - Both Sql Server and Sql lite databases are supported (Check installation instrcutions for more details)
- [Identity Server 4](http://identityserver.io/)
- [Angular 11](https://angular.io/)
- [Angular CLI 11](https://cli.angular.io/)
- Secure - with CSP and custom security headers
- [SignalR](https://github.com/aspnet/SignalR/)
- [SASS](http://sass-lang.com/)
- Best [practices](https://angular.io/docs/ts/latest/guide/style-guide.html) for Angular code organisation.
- [Clean Architecture](https://github.com/jasontaylordev/CleanArchitecture) inspired from Jason Taylor.
- [PWA support](https://developers.google.com/web/progressive-web-apps/)
- Fast Unit Testing with [Jest](https://facebook.github.io/jest/).
- E2E testing with [Protractor](http://www.protractortest.org).l
- [Compodoc](https://compodoc.github.io/compodoc/) for Angular documentation
- Login and Registration functionality using [Identity Server implicit flow](http://identityserver.io/)
- Extensible User/Role identity implementation
- Social logins support with token based authentication, using [Identity Server](http://identityserver.io/)
- Angular dynamic forms for reusable and DRY code.
- [Swagger](http://swagger.io/) as Api explorer (Visit url **https://127.0.0.1:5005/swagger** OR whatever port visual studio has launched the website.). More [details](https://github.com/domaindrivendev/Swashbuckle.AspNetCore)

## Pre-requisites

1. [.Net 5.0 SDK](https://www.microsoft.com/net/core#windows)
2. [Visual studio 2019](https://www.visualstudio.com/) OR [VSCode](https://code.visualstudio.com/) with [C#](https://marketplace.visualstudio.com/items?itemName=ms-vscode.csharp) extension
3. [NodeJs](https://nodejs.org/en/) (Latest LTS)
4. [Microsoft SQL Server](https://www.microsoft.com/en-us/sql-server) (Optional: If MS SQL server required instead of Sqlite during development)
5. [Docker](https://www.docker.com/) (Optional: If application will run inside docker container)

## TODOs

✓ ~~Create a Windows server instance in Azure. This is for running the build & the web back-end.~~ 
  Used local Virtualbox Win10 VM instaed of Azure instance.

▢ ~~Create bitbucket pipeline invoking Ansible for running the build in the remote machines.~~
  Removing it since we are using GoCD instead of Bitbucket pipelines.
 
✓ ~~Create GoCD pipeline invoking Ansible for running the build in the remote machines.~~
  This GoCD pipeline definition is available with name: `dotnetcore-clone-bld` in `gocd/pipelines` folder of this repo.

✓ ~~Create Ansible playbook to checkout & build the source code. This should build both the back-end  API and front-end Angular code.~~
  Used [this repo](https://github.com/KevinDockx/DocumentingAspNetCoreApisWithOpenAPI) instead of the sample application in `./src` folder.

❑ Write ansible playbook to upload artifacts to the Azure blob storage & publish as a static web site.

❑ Write module/role for invoking the Protractor based test suite for end-to-end testing.

